﻿using System;
using System.Collections.Generic;
using System.Text;

namespace BlazorDB.Model
{
    public class User
    {
        public string name { get; set; }
        public string lastName { get; set; }
        public  DateTime birthday { get; set; }
        public string email { get; set; } 
        public string country_id { get; set; }
        public string city { get; set; }
        public string gender { get; set; }
        public string password { get; set; }
    }
}
