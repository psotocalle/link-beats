﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using BlazorDB.Model;
using Linking.Data.Dapper.Repositorios;
using LinkingRythyms.Data;
using LinkingRythyms.Interfaces;

namespace LinkingRythyms.Servicios
{
    public class userService : IuserService
    {

        private readonly SqlConfiguration _configuration;
        private IUserR _userrepository;

        public userService(SqlConfiguration configuration)
        {
            _configuration = configuration;
            _userrepository = new UserRepository(configuration.ConnectionStrings);
        }
        public Task<IEnumerable<User>> GetAllUsers()
        {
            throw new NotImplementedException();
        }

        public Task<bool> SaveUser(User user)
        {
     
                return _userrepository.InsertUser(user);
         
        }
    }
}
