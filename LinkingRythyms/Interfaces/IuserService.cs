﻿using BlazorDB.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace LinkingRythyms.Interfaces
{
    interface IuserService
    {
        Task<IEnumerable<User>> GetAllUsers();

        Task<bool> SaveUser(User user);
    }
}
