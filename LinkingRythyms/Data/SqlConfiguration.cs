﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace LinkingRythyms.Data
{
    public class SqlConfiguration
    {
        public SqlConfiguration(string conectionString) => ConnectionStrings = conectionString;

            public string ConnectionStrings { get; }
    }
}
