USE [Linking-Beats]


CREATE TABLE dbo.lookup_countries(
	country_id    CHAR(2)         NOT NULL
  , country       NVARCHAR(100)   NOT NULL
  , CONSTRAINT pk_lookup_countries PRIMARY KEY CLUSTERED (country_id)
);

CREATE TABLE [dbo].[users](
  	[email]			NVARCHAR(255)				NOT NULL
  , [name]			NVARCHAR(200)				NOT NULL
  , [lastName]		NVARCHAR(200)				NOT NULL
  , [birthday]		DATE						NOT NULL
  , [country_id]	CHAR(2)                     NOT NULL
  ,	[city]			NVARCHAR(50)				NOT NULL
  , [password]	    NVARCHAR(MAX)               NOT NULL
  , CONSTRAINT pk_users PRIMARY KEY CLUSTERED (email ASC) 
  , CONSTRAINT fk_users_lookup_countries FOREIGN KEY (country_id) REFERENCES lookup_countries(country_id)
)

