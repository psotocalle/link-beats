﻿using BlazorDB.Model;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace Linking.Data.Dapper.Repositorios
{
    public interface IUserR
    {
        Task<IEnumerable<User>> GetAllUsers();
        Task<bool> InsertUser(User user);

        Task<bool> UpdateUser(User user);
    }
}
