﻿using BlazorDB.Model;
using Dapper;
using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Text;
using System.Threading.Tasks;

namespace Linking.Data.Dapper.Repositorios
{
    public class UserRepository : IUserR
    {


        private string ConnectionStrings;

        public UserRepository(string connectionString)
        {
            ConnectionStrings = connectionString;
        }

        protected SqlConnection dbConnection()
        {
            return new SqlConnection(ConnectionStrings);
        }


        public Task<IEnumerable<User>> GetAllUsers()
        {
            throw new NotImplementedException();
        }

        public async Task<bool> InsertUser(User user)
        {
            var db = dbConnection();

            var sql = @"INSERT INTO dbo.users (name, lastName, birthday, email,  country_id, city, gender, password)
                        VALUES (@name, @lastName, @birthday, @email, @country_id, @city, @gender, @password)";

            var result = await db.ExecuteAsync(sql.ToString(), new {user.name, user.lastName, user.birthday,
                                                                     user.email, user.country_id, user.city, user.gender, user.password});

            return result > 0;
        }

        public Task<bool> UpdateUser(User user)
        {
            throw new NotImplementedException();
        }
    }
}
